package com.dynamiccompany.actions;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

/**
 * Created by baptiste on 16/01/17.
 */
@Namespace("/")
@Results(
        {@Result(name = "success", location = "/index.jsp")}
        )
public class WelcomeAction extends ActionSupport {

    private String username;

    @Action("/welcome")
    public String execute() {
        return SUCCESS;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
