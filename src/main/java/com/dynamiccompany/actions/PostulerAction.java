package com.dynamiccompany.actions;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

/**
 * Created by baptiste on 17/01/17.
 */
@Namespace("/")
@Results(
        {@Result(name = "success", location = "/jsp/postuler.jsp")}
)
public class PostulerAction extends ActionSupport {

    @Action("/postuler")
    public String exectue() {
        return SUCCESS;
    }
}
