package com.dynamiccompany.actions;

import com.dynamiccompany.dto.Application;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import com.opensymphony.xwork2.ModelDriven;

/**
 * Created by baptiste on 17/01/17.
 */
@Namespace("/")
@Results({
        @Result(name = "success", location = "/jsp/postuler_success.jsp"),
        @Result(name = "input", location = "/jsp/postuler.jsp")
})
public class SaveAction extends ActionSupport implements ModelDriven<Application> {

    private Application application = new Application();

    @Action("/save")
    public String exectue() {
        return SUCCESS;
    }

    public void validate() {
        if (StringUtils.isBlank(application.getFirstname())) {
            addFieldError("firstname", "This field cannot be blank");
        }
        if (StringUtils.isBlank(application.getEmail())) {
            addFieldError("email", "This field cannot be blank");
        }

    }

    @Override
    public Application getModel() {
        return application;
    }

}
