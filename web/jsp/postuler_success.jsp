<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<s:head/>
<sj:head/>
<sb:head/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<s:url action="postuler" var="urlPostuler" />
<s:url action="welcome" var="urlHome" />
<body>
<div class="container">

  <!-- Static navbar -->
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">My company</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="<s:property value="urlHome" />">Home</a></li>
          <li><a href="#">About</a></li>
          <li class="active"><a href="<s:property value="urlPostuler" />">Nous rejoindre</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="../navbar-fixed-top/">Admin</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
  </nav>

  <!-- Main component for a primary marketing message or call to action -->
      
      <!-- Jumbotron -->
      <div class="jumbotron">
        <p class="alert alert-success">Votre candidature a bien été envoyée.</p>
          <p><strong>Email :</strong><s:property value="email"/></p>
          <p><strong>Firstname :</strong><s:property value="firstname"/></p>
          <p><strong>Lastname :</strong><s:property value="lastname"/></p>
          <p><strong>Motivation :</strong><s:property value="motivation"/></p>
          <p><strong>Contract :</strong><s:property value="contract"/></p>
          <p><strong>Places :</str  ong>
              <s:property value="towns"/>
          </p>
          <p><strong>Experience :</strong><s:property value="experience"/></p>
      </div>

    </div> <!-- /container -->
</body>
</html>	